import codecs
import time
import math
import os
import sys
import resource
import warnings
import luigi
from sklearn.metrics import f1_score
import numpy as np
import torch
import torch.utils.data as pdat
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils.rnn as rnnut
import pytorch_utils as putil

warnings.filterwarnings("ignore", message='Parameter "task_process_context" with value "None"')
# todo fasttext
# todo compare accuracy when using cross entropy loss/classification setup
LOG = putil.setup_logging()
OOV = "<OOV>"
PAD = "<PAD>"
EPOCH_EVENT = putil.Event([putil.LossPlotter().listen])
DEVSET_EVENT = putil.Event([putil.LossPlotter("devloss.svg").listen])
METRIC = nn.CrossEntropyLoss() #putil.RMSELoss() #lambda pred, tgt: f1_score(tgt, putil.softmax_to_prediction(pred), average="micro")  #

class IMDBDataSetSentence(pdat.Dataset):

    def __init__(self, filename, embeddict, transform=None, max_count=None, max_sents=100):
        self.text = []
        self.labels = []
        self.max_count = max_count
        self.embeddict = embeddict
        self.max_sents = max_sents

        with codecs.open(filename, encoding='utf-8') as infile:
            for line in infile:
                if self.max_count is not None and self.max_count <= len(self.text):
                    break
                if line.strip():
                    parts = line.split('\t\t')
                    self.labels.append(int(parts[2])-1)
                    sents = parts[3].split("<sssss>")
                    tokens = filter(lambda e: e, map(lambda s: list(filter(lambda w: w.lower() in embeddict, s.split())), sents))
                    tokens = list(sorted(tokens, key=len, reverse=True))
                    self.text.append(tokens)

    def __len__(self):
        return len(self.text)

    def get_word(self, wrd):
        if wrd.lower() in self.embeddict:
            return torch.tensor(self.embeddict[wrd.lower()])
        else:
            return torch.tensor(self.embeddict[OOV])

    def __getitem__(self, index):
        review = []
        sents = self.text[index]
        lengths = []
        firstlen = None
        for idx, se in enumerate(sents):
            if idx == self.max_sents:
                break
            lengths.append(len(se))
            if firstlen is None:
                firstlen = len(se)
            review.append(putil.pad_tensor(torch.stack(list(map(self.get_word, se))), firstlen, 0))
        lengths.extend(0 for k in range(self.max_sents-idx-1))
        review.extend([ torch.zeros((firstlen, len(self.get_word(OOV) ))) for k in range(self.max_sents-idx-1) ])
        return (torch.stack(review), lengths, self.labels[index])
        
class LSTMHierarchical(nn.Module):
    def __init__(self, input_dim, hidden_dim, label_cnt):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.wordlstm = nn.LSTM(input_dim, hidden_dim, batch_first=True)
        self.avgsen = nn.AdaptiveAvgPool2d((1, None))
        self.senlstm = nn.LSTM(hidden_dim, hidden_dim, batch_first=True)
        self.avgdoc = nn.AdaptiveAvgPool2d((1, None))
        self.out = nn.Linear(hidden_dim, hidden_dim)
        self.predict = nn.Linear(hidden_dim, label_cnt)
        self.hidden_params = None
        self.dropout = nn.Dropout(p=0.3)

    def init_hidden(self, cur_batch_size):
        return (torch.zeros(1, cur_batch_size, self.hidden_dim),
                torch.zeros(1, cur_batch_size, self.hidden_dim))

    def forward(self, review, lngth):
        res = []
        for i, r in enumerate(review):
            self.hidden_params = self.init_hidden(len(lngth[i][lngth[i] > 0]))
            btc = r[lngth[i] > 0]
            packed = rnnut.pack_padded_sequence(btc, lngth[i][lngth[i] > 0], batch_first=True)
            lstm_out, self.hidden_params = self.wordlstm(
                packed, self.hidden_params)
            outs, ln = rnnut.pad_packed_sequence(lstm_out, batch_first=True)
            #averaged = self.avgsen(outs).transpose(1, 0) # now we want the sentences
            averaged = torch.sum(outs, dim=1)/torch.sum(ln.float())
            lstm_out, self.hidden_params = self.senlstm(self.dropout(averaged.unsqueeze(0)), self.init_hidden(1))
            averaged_doc = self.avgdoc(lstm_out)
            tag_space = self.out(self.dropout(averaged_doc))
            tag_scores = F.relu(tag_space)
            res.append(self.predict(tag_scores).view(-1))
            
        return torch.stack(res)

def run_batch(model, optimizer, criterion, batch, lables, lengths=None):
    start = time.time()
    model.train()
    optimizer.zero_grad()
    # if lengths is not None:
    #     sortedall = putil.sort_lengths_desc(lengths, batch, lables)
    #     batch = sortedall[1]
    #     lengths = sortedall[0]
    #     lables = sortedall[2]
    outputs = model(batch, lengths)
    loss = criterion(outputs, lables)
    loss.backward()
    optimizer.step()
    LOG.debug("loss %f time %.2f", loss, time.time()-start)
    return loss.detach().numpy()

def run_epoch(model, optimizer, criterion, current_epoch, data_loader, save_checkpoint=lambda x: False, dev_set=None, log_info=False):
    start = time.time()
    totloss = 0
    batchcount = 0
    LOG.debug("Starting epoch %d", current_epoch)
    for (batch, lengths, labels) in data_loader:
        #labels = labels.float()
        totloss += run_batch(model, optimizer, criterion, batch, labels, lengths)
        batchcount += 1

    avgloss = totloss/batchcount
    if log_info:
        LOG.info("epoch %d with loss %f and duration %d using cpu mem %d", current_epoch, avgloss, time.time()-start, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
    else:
        LOG.debug("epoch %d with loss %f and duration %d using cpu mem %d", current_epoch, avgloss, time.time()-start, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)

    metric = None
    if dev_set is not None:
        metric = evaluate(model, dev_set, METRIC)
        LOG.info(putil.bcolors.OKBLUE +"total eval on dev: %f"+putil.bcolors.ENDC, metric)
        DEVSET_EVENT("Dev", metric, current_epoch)
        model.train()
    if save_checkpoint(metric):
        LOG.info(putil.bcolors.OKGREEN+"saving model with avg train loss %f in epoch %d"+putil.bcolors.ENDC, avgloss, current_epoch)
        torch.save(model.state_dict(), "cpsave_l_%.2f_e_%d.model" % (avgloss if metric is None else metric, current_epoch))
    EPOCH_EVENT("Epoch", avgloss, current_epoch)
    return avgloss

def run_training(model, optimizer, criterion, dataset, batch_size, epoch_count=10, dev_set=None, save_every=4, log_every=1):
    data_loader = pdat.DataLoader(dataset, batch_size=batch_size, shuffle=True, collate_fn=putil.PadCollate(1))
    start = time.time()
    for epoch in range(1, epoch_count+1):
        save_fun = lambda x: epoch % save_every == 0
        avgloss = run_epoch(model, optimizer, criterion, epoch, data_loader, save_checkpoint=save_fun, dev_set=dev_set if save_fun(99) else None, log_info=epoch % log_every == 0)
    duration = time.time() - start
    params_trained = sum(p.numel() for p in model.parameters() if p.requires_grad)
    DEVSET_EVENT("COMPLETE")
    EPOCH_EVENT("COMPLETE")
    LOG.info("Concluded training %d params for %d epochs on %d elements after %d seconds (e/s %.2f) with final avg loss %f", params_trained, epoch_count, len(dataset), duration, len(dataset)*epoch_count/duration, avgloss)
    for para in model.parameters():
        LOG.debug(para.shape)
        LOG.debug(para.data)
    return model

def evaluate(model, dataloader, metric, agg=sum, initial_total=0):
    model.eval()
    total = initial_total
    with torch.no_grad():
        corr = 0
        tot = 0
        for (batch, lengths, lables) in dataloader:
            #lables = lables.float()
            if lengths is not None:
                pass
                # sortedall = putil.sort_lengths_desc(lengths, batch, lables)
                # batch = sortedall[1]
                # lengths = sortedall[0]
                # lables = sortedall[2]
                # texts = []
                # for idx in sortedall[3]:
                #     texts.append(dataloader.dataset.text[idx])
            out = model(batch, lengths)
            mtr = metric(out, lables)
            tot += len(out)
            for i, r in enumerate(out):
                pred = torch.argmax(r, 0)
                if pred == lables[i]:
                    corr += 1
                # if r < -2 or r > 3:
                #     print(r)
                #     print(lables[i])
                #     print(" ".join(map(lambda x: " ".join(x), dataloader.dataset.text[i])))
            LOG.debug("data loss in batch %f", mtr)
            total = agg([total, mtr])
        print("acc %.3f" % (corr/tot))
    return total/len(dataloader)

class HierarchicalLstmTask(luigi.Task):
    training_examples_count = luigi.IntParameter(default=1000)
    batch_size = luigi.IntParameter(default=32)
    embedding_size = luigi.IntParameter(default=100)
    hidden_size = luigi.IntParameter(default=200)
    epoch_count = luigi.IntParameter(default=10)
    loss = luigi.Parameter(default="CrossEntropyLoss")
    optim = luigi.Parameter(default="Adam")
    run_nr = luigi.IntParameter(default=0)

    def output(self):
        return luigi.LocalTarget("intermediateData/"+putil.params_to_filename(self))

    def run(self):
        glove = putil.load_embedding_dict('/home/vncuser/Downloads/glove.6B/glove.6B.'+ str(self.embedding_size) +'d.txt')
        dataset = IMDBDataSetSentence('/data/movieandyelpreviews/IMDB/train.txt', glove, max_count=self.training_examples_count)
        devset = IMDBDataSetSentence('/data/movieandyelpreviews/IMDB/test.txt', glove, max_count=10000)
        devset_loader = pdat.DataLoader(devset, batch_size=128, collate_fn=putil.PadCollate(1))
        model = LSTMHierarchical(self.embedding_size, self.hidden_size, 10)
        criterion = getattr(nn, self.loss)()
        optimizer = getattr(optim, self.optim)(model.parameters(), lr=0.001)
        LOG.info("Data loaded starting training for %s", self)
        run_training(model, optimizer, criterion, dataset, self.batch_size, epoch_count=self.epoch_count, dev_set=devset_loader, save_every=3)
        tmp_loc = "intermediateData/tmp.model"
        evaluate(model, devset_loader, printmetric)
        torch.save(model.state_dict(), tmp_loc)
        os.rename(tmp_loc, self.output().path)

def printmetric(pred, target):
    print(torch.stack((torch.argmax(pred, dim=1), target)))
    return 0

if __name__ == "__main__":
    glove = putil.load_embedding_dict('/home/vncuser/Downloads/glove.6B/glove.6B.100d.txt')
    devset = IMDBDataSetSentence('/data/movieandyelpreviews/IMDB/dev.txt', glove, max_count=10000)
    devset_loader = pdat.DataLoader(devset, batch_size=128, collate_fn=putil.PadCollate(1))
    mdl = LSTMHierarchical(100, 200, 10)
    mdl.load_state_dict(torch.load(sys.argv[1]))
    print(evaluate(mdl, devset_loader, METRIC))
