"""Utilities for pytorch"""
import logging
import codecs
import subprocess
import torch
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tick

class IndexMaker():
    """Make word to index dict as implicite one hot encoding"""
    def __init__(self, specialTokens=[]):
        self.nextidx = 0
        self.word_to_index = {}
        self.frozen = False
        for tok in specialTokens:
            self.word_to_index[tok] = self.nextidx
            self.nextidx += 1

    def add_word(self, word):
        if word not in self.word_to_index:
            if self.frozen:
                raise Exception("already frozen")

            self.word_to_index[word] = self.nextidx
            self.nextidx += 1

    def freeze_encodings(self):
        self.frozen = True
        return self.word_to_index

def sort_lengths_desc(lengths, *to_be_sorted):
    lng, resort_idx = lengths.sort(descending=True)
    res = [lng]
    for item in to_be_sorted:
        res.append(item[resort_idx])
    res.append(resort_idx)
    return res

def setup_logging():
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT, level=logging.INFO)
    return logging.getLogger('pytorch')

def params_to_filename(*tasks, prefix="mdl", use_commit=True):
    fname = prefix
    for task in tasks:
        for pname in task.get_param_names():
            fname += "-" + pname + "_" + str(getattr(task, pname))
    if use_commit:
        fname += "-"+subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).decode().strip()
    return fname+".model"

def softmax_to_prediction(out):
    return torch.argmax(out, dim=1)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# from https://github.com/A-Jacobson/CNN_Sentence_Classification/blob/master/WordVectors.ipynb
def load_txt_embeddings(path, word2idx, embedding_dim=50):
    with codecs.open(path, encoding="utf-8") as f:
        cnt = 0
        embeddings = np.zeros((len(word2idx), embedding_dim))
        for line in f.readlines():
            values = line.split()
            word = values[0]
            index = word2idx.get(word)
            if index:
                vector = np.array(values[1:], dtype='float32')
                embeddings[index] = vector
                cnt += 1
        print(len(word2idx))
        print(cnt)
        return torch.from_numpy(embeddings).float()

def load_embedding_dict(path, oov="<OOV>"):
    """
    creates a dictionary mapping words to vectors from a file in glove format.
    """
    with codecs.open(path, encoding="utf-8") as f:
        glove = {}
        for line in f.readlines():
            values = line.split()
            word = values[0]
            vector = np.array(values[1:], dtype='float32')
            glove[word] = vector

        glove[oov] = np.random.randn(len(values)-1).astype(dtype='float32')

        return glove

class RMSELoss(torch.nn.MSELoss):
    def forward(self, input, target):
        return torch.sqrt(super(RMSELoss, self).forward(input, target))

def save_best(init_best=0.0):
    best = init_best
    def is_better(cur):
        nonlocal best
        if cur > best:
            best = cur
            return True
        else:
            return False
    return is_better

class Event(list):
    def __call__(self, *args, **kwargs):
        for listener in self:
            listener(*args, **kwargs)

    def __repr__(self):
        return "Event(%s)" % list.__repr__(self)

class LossPlotter():
    def __init__(self, name="loss.svg"):
        self.losses = []
        self.name = name

    def listen(self, event_type, loss=0.0, epoch_nr=0):
        if event_type == "COMPLETE":
            fig: plt.Figure
            ax: plt.Axes
            fig, ax = plt.subplots(1, 1)
            ax.plot(self.losses)
            ax.set_yscale('log')
            ax.yaxis.set_major_locator(tick.LinearLocator())
            ax.yaxis.set_major_formatter(tick.FormatStrFormatter("%.3f"))
            ax.yaxis.set_minor_formatter(tick.NullFormatter())
            plt.savefig(self.name, bbox_inches='tight')
            fig.clf()
        else:
            self.losses.append(float(loss))

def pad_tensor(vec, pad, dim):
    """
    args:
        vec - tensor to pad
        pad - the size to pad to
        dim - dimension to pad

    return:
        a new tensor padded to 'pad' in dimension 'dim'
    """
    pad_size = list(vec.shape)
    pad_size[dim] = pad - vec.size(dim)
    return torch.cat([vec, torch.zeros(*pad_size)], dim=dim)


class PadCollate:
    """
    a variant of callate_fn that pads according to the longest sequence in
    a batch of sequences
    """

    def __init__(self, dim=0):
        """
        args:
            dim - the dimension to be padded (dimension of time in sequences)
        """
        self.dim = dim

    def pad_collate(self, batch):
        """
        args:
            batch - list of (tensor, *label)

        reutrn:
            xs - a tensor of all examples in 'batch' after padding
            ys* - a LongTensor for each additional arg/label in batch as long tensor
        """
        # find longest sequence
        max_len = max(map(lambda x: x[0].shape[self.dim], batch))
        # pad according to max_len
        batch = list(map(lambda arg:
                    (pad_tensor(arg[0], pad=max_len+1, dim=self.dim), *arg[1:]), batch))
        # stack all
        xs = torch.stack(list(map(lambda x: x[0], batch)), dim=0)
        ys = []
        for ix in range(1, len(batch[0])):
            ys.append(torch.LongTensor(list(map(lambda b: b[ix], batch ))))
        return (xs, *ys)

    def __call__(self, batch):
        return self.pad_collate(batch)

