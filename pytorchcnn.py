
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as dut
import torch.nn.utils.rnn as rnnut
import pandas as pd
import codecs
import time
import resource

def batch_size_64():
    return 64

setsize = 60000
BATCH_SIZE = batch_size_64()

class IMDBDataSet(dut.Dataset):
    def __init__(self, fln, transform=None, wordtoix = None):
        self.text = []
        self.labels = []
        vocab = set([])
        vocab.add("<PAD>")
        i = 0
        with codecs.open(fln, encoding='utf-8') as inf:
            for line in inf:
                if i > setsize:
                    break
                if len(line.strip()) > 0:
                    i += 1
                    parts = line.split('\t\t')
                    self.labels.append(int(parts[2])-1)
                    tokens = parts[3].split()
                    self.text.append(tokens)
                    for w in tokens:
                        if wordtoix:
                            pass
                        else:
                            vocab.add(w)

        if wordtoix is None:
            self.word_to_ix = {word: i for i, word in enumerate(vocab)}
        else:
            self.word_to_ix = wordtoix


    def __len__(self):
        return len(self.text)

    def get_word(self, w):
        if w in self.word_to_ix:
            return self.word_to_ix[w]
        else:
            return self.word_to_ix["<PAD>"]

    def __getitem__(self, index):
        res = list(map(self.get_word, self.text[index]))
        origlen = len(res)
        length = 512
        rest = length - len(res)
        res.extend([self.word_to_ix["<PAD>"] for i in range(rest) ])
        return (torch.tensor(res[:length]), min(length, origlen), self.labels[index])

class CNNBare(nn.Module):
    def __init__(self, vocab_size, input_dim, hidden_dim, label_cnt, pad_idx=None):
        super().__init__()
        conv_filters = 60
        self.embedding_size = input_dim
        self.embedding = nn.Embedding(vocab_size, input_dim, scale_grad_by_freq=False, padding_idx=pad_idx)
        self.cnn = nn.Conv2d(1, conv_filters, (5, input_dim))
        self.pool = nn.AdaptiveMaxPool2d((hidden_dim, None))
        self.cnn2 = nn.Conv2d(1, conv_filters, (3, input_dim))
        self.intermediate = nn.Linear(hidden_dim*conv_filters*2, hidden_dim)
        #self.intermediate = nn.Linear(hidden_dim*conv_filters, hidden_dim)
        self.out = nn.Linear(hidden_dim, label_cnt)

    def forward(self, review, lngth):
        embeds = self.embedding(review).view(BATCH_SIZE, 1, -1, self.embedding_size)
        #print(embeds.shape)
        cnnout = self.cnn(F.relu(embeds))
        #print(cnnout.shape)
        cnnout2 = self.cnn2(F.relu(embeds.narrow(2, 1, 510)))
        #print(cnnout2.shape)
        pool = self.pool(F.relu( torch.cat( (cnnout, cnnout2), dim=3 )))
        #pool = self.pool(F.relu(cnnout))
        #print(pool.shape)
        hidden_space = self.intermediate(pool.view(BATCH_SIZE, -1))
        tag_space = self.out(F.relu(hidden_space))
        tag_scores = F.softmax(tag_space, dim=1)
        return tag_scores

if __name__ == "__main__":
    dataset = IMDBDataSet('/data/movieandyelpreviews/IMDB/train.txt')
    model =  CNNBare(len(dataset.word_to_ix), 100, 180, 10, dataset.word_to_ix["<PAD>"])
    dataLoader = dut.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adagrad(model.parameters())
    firststart = time.time()
    start = firststart
    estart = start
    for e in range(30):
        totloss = 0
        print("e="+str(e))
        for (btc, lng, lbl) in dataLoader:
            model.train()
            optimizer.zero_grad()
            outputs = model(btc, lng)
            loss = criterion(outputs, lbl)
            totloss += loss
            loss.backward()
            optimizer.step()
            end = time.time()
            print("loss {} time {} e {}".format(loss, end-start, e))
            start = end
        print("avg loss {} time {} mem {}".format(totloss/(setsize/BATCH_SIZE), end-estart, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss))
        estart = end
    print("Total training time {}".format(time.time() - firststart))
    model.zero_grad()
    for para in model.parameters():
        print(para.shape)
        print(para.data)
    torch.save(model.state_dict(), 'alpha.model')
    testset = IMDBDataSet('/data/movieandyelpreviews/IMDB/test.txt', wordtoix=dataset.word_to_ix)
    dataLoaderTest = dut.DataLoader(testset, batch_size=BATCH_SIZE, drop_last=True)
    model.load_state_dict(torch.load('alpha.model'))
    for i, (btc, lng, lbl) in enumerate(dataLoaderTest):
        model.eval()
        with torch.no_grad():
            out = model(btc, lng)
            prediction = torch.argmax(out, dim=1)
            print(prediction)
            print(lbl)
            for e, p in enumerate(prediction):
                if p < 5:
                    print(p)
                    print(" ".join(testset.text[i*2+e]))

        print("--"+str(i)+"--")
        